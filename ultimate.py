import sys



class piece(object):
	def __init__(self,id,up,down,left,right):
		self.id = id
		self.up = up
		self.down = down
		self.left = left
		self.right = right
	
	def rotate( self ):
		tempUp = self.up
		tempDown = self.down
		tempLeft = self.left
		tempRight = self.right
		self.up = tempLeft
		self.down = tempRight
		self.left = tempDown
		self.right = tempUp
	
	def flip( self ):
		tempLeft = self.left
		tempRight = self.right
		self.left = tempRight
		self.right = tempLeft
		
	def __repr__(self):
		return self.id

def match(sideone,sidetwo):
	one = sideone.split("_")
	two = sidetwo.split("_")
	if(one[0] == two[0]):
		if(one[1] == "o" and two[1] == "i"):
			return 1
		elif(one[1] == "i" and two[1] == "o"):
			return 1
		else:
			return 0
	else:
		return 0
		
def check(piece,left,above):
	leftOkay = 0
	upOkay = 0
	if(left != 0):
		if(match(piece.left,left.right) == 1):
			leftOkay = 1
		if(above == 0):
			return leftOkay
	if(above != 0):
		if(match(piece.up,above.down) == 1):
			upOkay = 1
		if(left == 0):
			return upOkay
	return leftOkay * upOkay #if either is 0 return a false
	
def solve(n, remain, placed):
	#The very start, we can just place without check
	if(n == 0):
		length = len(remain)
		for x in range(length):
			#Place the piece
			tempL = remain[x:x+1]
			temp = tempL.pop(0)
			
		
			for i in range(0,4): #4 rotations
				#place the piece
				placed.append(temp)
				placedCopy = []
				placedCopy = list(placed)
				remainCopy = []
				remainCopy = list(remain)
				remainCopy.pop(x)
				#descend down to next piece given rotation
				catch = solve(1,remainCopy,placedCopy)	
				temp.rotate()
				placed.pop(0)
				
				temp.flip()
				placed.append(temp)
				placedCopy = []
				placedCopy = list(placed)
				remainCopy = []
				remainCopy = list(remain)
				remainCopy.pop(x)
				catch = solve(1,remainCopy,placedCopy)
				temp.rotate()
				placed.pop(0)
					
	else:
		length = len(remain)
		for y in range(length): 
			tempL = remain[y:y+1]
			temp = tempL.pop(0)
			
			unrotatedL = remain[y:y+1]
			unrotated = unrotatedL.pop(0)
			
			fit = 0
			for i in range(0,4):
				#Check if we can place
				okay = 0
				#print(temp.id,temp.up,temp.down,temp.left,temp.right)
				if(n > 0 and n < 4): #Check left only
					okay = check(temp,placed[n-1],0)
				elif(n == 4 or n == 8 or n == 12): #Check up only
					okay = check(temp,0,placed[n-4])
				elif((n > 4 and n < 8) or (n > 8 and n < 12) or (n > 12 and n < 16)): #Check both
					okay = check(temp,placed[n-1],placed[n-4])
					
					
				#If we can place it
				if(okay == 1):
					#If we're on this depth then we have the solution n = 0 is 1 placed n = 15 is 16 placed
					if(n == 15):
						print("Reached solution:")
						for i in range(len(placed)):
							print(i+1)
							print(placed[i].id,placed[i].up,placed[i].down,placed[i].left,placed[i].right)
						print("16")
						print(temp.id,temp.up,temp.down,temp.left,temp.right)
						sys.exit() #disable this comment if you just want the first available solution
						
					placedCopy = []
					placedCopy = list(placed)
					placedCopy.append(temp)

					remainCopy = []
					remainCopy = list(remain)
					remainCopy.pop(y)

					catch = solve(n+1,remainCopy,placedCopy) #Go to next piece
				
				
				#The flip of each of the four rotations, perform same check as above
				temp.flip() #Flip the piece so we can check the other side
					
					
				okay = 0
				if(n > 0 and n < 4): #Check left only
					okay = check(temp,placed[n-1],0)
				elif(n == 4 or n == 8 or n == 12): #Check up only
					okay = check(temp,0,placed[n-4])
				elif((n > 4 and n < 8) or (n > 8 and n < 12) or (n > 12 and n < 16)): #Check both
					okay = check(temp,placed[n-1],placed[n-4])
					
					
				#If we can place it
				if(okay == 1):
					#If we're on this depth then we have the solution n = 0 is 1 placed n = 15 is 16 placed
					if(n == 15):
						print("Reached solution:")
						for i in range(len(placed)):
							print(i+1)
							print(placed[i].id,placed[i].up,placed[i].down,placed[i].left,placed[i].right)
						print("16")
						print(temp.id,temp.up,temp.down,temp.left,temp.right)
						sys.exit()
						
					placedCopy = []
					placedCopy = list(placed)
					placedCopy.append(temp)

					remainCopy = []
					remainCopy = list(remain)
					remainCopy.pop(y)

					catch = solve(n+1,remainCopy,placedCopy) #Go to next piece
				temp.flip()
				temp.rotate()
	return n
	
if __name__ == "__main__":

	highestDepth = 0
	placed = []
	
	one = piece(1,"ao_o","c_i","ao_o","ai_i")
	two = piece(2,"ao_o","ai_i","t_i","t_o")
	three = piece(3,"ai_o","t_i","ai_o","c_i")
	four = piece(4,"t_o","ai_i","ao_i","ai_o")
	five = piece(5,"c_i","c_o","t_i","ai_o")
	six = piece(6,"c_i","ao_o","ao_i","c_o")
	seven = piece(7,"t_i","c_o","c_i","t_o")
	eight = piece(8,"ao_i","ai_o","ao_i","c_o")
	nine = piece(9,"ao_i","ai_o","ai_o","t_i")
	ten = piece(10,"c_o","ao_i","c_o","c_i")
	eleven = piece(11,"ao_i","c_o","t_o","ao_i")
	twelve = piece(12,"c_i","ao_o","c_o","t_i")
	thirteen = piece(13,"ai_i","ai_o","c_o","t_i")
	fourteen = piece(14,"t_o","c_i","c_o","ai_i")
	fifteen = piece(15,"ao_o","c_i","ai_o","ao_i")
	sixteen = piece(16,"ao_o","c_i","t_o","ao_i")
	game = []
	game.append(one)
	game.append(two)
	game.append(three)
	game.append(four)
	game.append(five)
	game.append(six)
	game.append(seven)
	game.append(eight)
	game.append(nine)
	game.append(ten)
	game.append(eleven)
	game.append(twelve)
	game.append(thirteen)
	game.append(fourteen)
	game.append(fifteen)
	game.append(sixteen)

	catch = solve(0,game,placed)	