Figuring out a puzzle my father received for Christmas in 2016 the best way.. 

BY MAKING A PROGRAM DO IT

16 pieces are to be arranged in a square, each piece has 4 shapes facing in/out so there
is no straight edge.

After coding in the 16 pieces it was a matter of recursion decending down paths for any chain 
of piece that did fit, each piece would have to be rotated 4 times for each attempt at placement
as well as flipped for each rotation.

There are 20 trillion permutations of 16. That's not really relevant but if you have the time and 
memory you could technically find a solution that way.